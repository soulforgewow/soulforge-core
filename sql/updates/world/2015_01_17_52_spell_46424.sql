-- Fix bad behavior of aura Power of the Storm (46424).

-- Remove script from creature.
UPDATE `creature_template` SET `AIName` = "" WHERE `entry` = 25982;

-- Remove casting of aura 46424. Handled in spell_area.
DELETE FROM `smart_scripts` WHERE `entryorguid` = 25982;
UPDATE `smart_scripts` SET `link` = 0 WHERE `entryorguid` = 26045 AND `id` = 7;
DELETE FROM `smart_scripts` WHERE `entryorguid` = 26045 AND `id` = 8;

-- Apply aura 46424 if quest Weakness to Lightning (11896) is in quest log.
DELETE FROM `spell_area` WHERE `spell` = 46424;
INSERT INTO `spell_area` VALUES
(46424, 4035, 11896, 0, 0, 0, 2, 1, 10, 0), -- The Geyser Fields
(46424, 4036, 11896, 0, 0, 0, 2, 1, 10, 0), -- Fizzcrank Pumping Station
(46424, 4146, 11896, 0, 0, 0, 2, 1, 10, 0), -- North Point Station
(46424, 4148, 11896, 0, 0, 0, 2, 1, 10, 0); -- South Point Station

