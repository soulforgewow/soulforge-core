-- Remove quest Pilgrim's Bounty from Blood Elf Commoner, so that it cannot be taken outside of the event timeframe.
DELETE FROM `creature_queststarter` WHERE `quest` = 14036;


-- Quest Pilgrim's Bounty (14036) is supposed to be Horde only.
UPDATE `quest_template` SET `RequiredRaces` = 690 WHERE `Id` = 14036;


-- Quest Pilgrim's Bounty (14022) is supposed to be Alliance only.
UPDATE `quest_template` SET `RequiredRaces` = 1101 WHERE `Id` = 14022;


-- Prevent Silithus bonfires to show outside of the Midsummer event timeframe.
DELETE FROM `game_event_gameobject` WHERE `guid` IN (49312, 49313);
INSERT INTO `game_event_gameobject` VALUES
(1, 49312),
(1, 49313);


-- Make Void Spawner (quest: Warp Rifts) invulnerable and not targetable
UPDATE `creature_template` SET `unit_flags` = 256 | 33554432 WHERE `entry` = 20143;


-- Make Magic Sucker Device Buttress (quest: The Demoniac Scryer) not targetable
UPDATE `creature_template` SET `unit_flags` = 33554432 WHERE `entry` = 22267;


-- Quest Venomhide Eggs requires completion of Toxic Tolerance
UPDATE `quest_template` SET `PrevQuestId` = 13850 WHERE `id` = 13887;


-- Hellscream's Vigil
DELETE FROM `conditions` WHERE `SourceEntry` IN (11585, 11586) AND `SourceTypeOrReferenceId` IN (19, 20);
INSERT INTO `conditions` VALUES 
(19, 0, 11586, 0, 0, 8, 0, 10212, 0, 0, 0, 0, 0, "", "Hellscream's Vigil (11586) Only if player has completed Hero of the Mag'har"),
(20, 0, 11586, 0, 0, 8, 0, 10212, 0, 0, 0, 0, 0, "", "Hellscream's Vigil (11586) Only if player has completed Hero of the Mag'har"),
(19, 0, 11585, 0, 0, 8, 0, 10212, 0, 0, 1, 0, 0, "", "Hellscream's Vigil (11585) Only if player has not completed Hero of the Mag'har"),
(20, 0, 11585, 0, 0, 8, 0, 10212, 0, 0, 1, 0, 0, "", "Hellscream's Vigil (11585) Only if player has not completed Hero of the Mag'har");


-- Move NPCs required for quest Scouting the Sinkholes so that they are above ground. They were too far away for the quest item to work.
UPDATE `creature` SET `position_z` = 10.0000 WHERE `id` IN (25664, 25665, 25666);

