DELETE FROM `rbac_permissions` WHERE `id` = 1000;
INSERT INTO `rbac_permissions` VALUES (1000, "Command: reload mail_custom_level_reward");

DELETE FROM `rbac_linked_permissions` WHERE `id` = 192 AND `linkedId` = 1000;
INSERT INTO `rbac_linked_permissions` VALUES (192, 1000);

