DELETE FROM `points_of_interest` WHERE `entry` BETWEEN 500 AND 509;
INSERT INTO `points_of_interest` VALUES
(500, 2164.6990, 1255.3929, 45, 75, 0, "King's Square", 0), -- Wave 1
(501, 2254.4343, 1163.4276, 45, 75, 0, "Festival Lane", 0), -- Wave 2
(502, 2348.1201, 1202.3025, 45, 75, 0, "Festival Lane", 0), -- Wave 3
(503, 2139.8252, 1356.2771, 45, 75, 0, "King's Square", 0), -- Wave 4
(504, 2264.0134, 1174.0559, 45, 75, 0, "Festival Lane", 0), -- Wave 6
(505, 2349.7017, 1188.4366, 45, 75, 0, "Festival Lane", 0), -- Wave 7
(506, 2145.2129, 1355.2881, 45, 75, 0, "King's Square", 0), -- Wave 8
(507, 2172.6863, 1259.6182, 45, 75, 0, "King's Square", 0), -- Wave 9
(508, 2191.7388, 1335.7274, 45, 75, 0, "King's Square", 0), -- Two bosses (waves 5/10)
(509, 2366.8958, 1199.2434, 45, 75, 0, "Festival Lane", 0); -- North area (after last wave)

