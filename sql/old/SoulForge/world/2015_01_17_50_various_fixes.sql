-- Allow item Arcane Disruptor (37888) to be used only near Surpicious Grain Crates (190117).
DELETE FROM `conditions` WHERE `SourceEntry` = 49590;

INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) values
('13','1','49590','0','0','31','0','3','27827','0','0','0','0','','Dispelling Illusions: Crate Dummy target'),
('17','0','49590','0','1','30','0','190117','8','0','0','172','68','','Dispelling Illusions: requires gameobject Suspicious Grain Crate within 8 yards');


-- Make Dead Mage Hunter (26477) despawn on spellclick.
UPDATE smart_scripts SET event_type = 73, event_param1 = 0 WHERE entryorguid = 26477 AND id = 0;


-- Fix gossip for Elegant Letter, NPC Keryn Sylvius (917).
SET @NPCEntry := 917;
SET @GossipMenuID := 381;

UPDATE `creature_template` SET `AIName` = "SmartAI" WHERE `entry` = @NPCEntry;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @NPCEntry;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(@NPCEntry,'0','0','1','62','0','100','0',@GossipMenuID,'4','0','0','72','0','0','0','0','0','0','7','0','0','0','0','0','0','0','Keryn Sylvius - On Gossip Select - Close gossip'),
(@NPCEntry,'0','1','0','61','0','100','0','0','0','0','0','11','21100','0','0','0','0','0','7','0','0','0','0','0','0','0','Keryn Sylvius - Linked with Previous Event - Cast Conjure Elegant Letter');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 15 AND `SourceGroup` = @GossipMenuID AND `SourceEntry` = 4;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('15',@GossipMenuID,'4','0','0','27','0','24','3','0','0','0','0','','Show gossip option if player is at least level 24'),
('15',@GossipMenuID,'4','0','0','2','0','17126','1','0','1','0','0','','Show gossip option if player does not have Elegant Letter'),
('15',@GossipMenuID,'4','0','0','15','0','8','0','0','0','0','0','','Show gossip option if player is a rogue'),
('15',@GossipMenuID,'4','0','0','8','0','6681','0','0','1','0','0','','Show gossip option if player is not rewarded for quest 6681'),
('15',@GossipMenuID,'4','0','0','9','0','6681','0','0','1','0','0','','Show gossip option if player does not have quest 6681 taken'),
('15',@GossipMenuID,'4','0','0','28','0','6681','0','0','1','0','0','','Show gossip option if player does not have quest 6681 complete');


-- Fix Instant Statue Pedestal.
UPDATE `creature_template` SET `vehicleid` = 732, `unit_flags` = 33554432 WHERE `entry` = 40246;

DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN (74890, -75731);
INSERT INTO `spell_linked_spell` VALUES 
(74890, 75055, 0, "Instant Statue"),
(-75731, -75055, 0, "Instant Statue");


-- Allow Wintergrasp quests to be completed while in raid.
UPDATE quest_template SET CompletionAllowedInRaid = 1 WHERE Id IN ('236', '13153', '13154', '13156', '13177', '13178', '13179', '13180', '13181', '13183', '13185', '13186', '13191', '13192', '13193', '13194', '13195', '13196', '13197', '13198', '13199', '13200', '13201', '13202', '13222', '13223', '13538', '13539');

