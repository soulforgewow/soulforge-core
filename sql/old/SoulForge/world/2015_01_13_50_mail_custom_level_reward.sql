DROP TABLE IF EXISTS `mail_custom_level_reward`;
CREATE TABLE `mail_custom_level_reward` (
`level` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
`raceMask` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0,
`subject` TEXT NOT NULL,
`text` TEXT NOT NULL,
`money` INT(10) UNSIGNED NOT NULL DEFAULT 0,
PRIMARY KEY (`level`, `raceMask`)
); 

